import React from 'react'
import { injectIntl } from 'react-intl'
import { Row, Column, Layout } from 'flex-layouts'
import { Block } from '@hl/ui/src/content'
import { Footer } from '@hl/ui/src/footer'
import { HelpIcon } from '@hl/ui/src/icons'
import { Text } from '@hl/ui/src/text'
import { Button } from '@hl/ui/src/button'
import messages from '../../messages'

const Main = ({
  intl,
}) => (
  <Block
    color='gray1200'
  >
    <Row fill>
      <Layout basis='10%' />
      <Column
        fill
      >
        <Layout grow={1} />
        <Layout
          justify='center'
        >
          <HelpIcon
            width={60}
          />
        </Layout>
        <Layout basis='20px' />
        <Layout
          justify='center'
        >
          <Text
            color='gray500'
            size='medium'
            weight='light'
          >
            {intl.formatMessage(messages.welcome)}
          </Text>
        </Layout>
        <Layout
          justify='center'
        >
          <Text
            color='gray500'
            size='medium'
            weight='light'
          >
            {intl.formatMessage(messages.dontHaveAnyCompany)}
          </Text>
        </Layout>
        <Layout basis='80px' />
        <Layout>
          <Row
            justify='center'
          >
            <Layout basis='400px'>
              <Button
                color='green'
              >
                {intl.formatMessage(messages.createCompany)}
              </Button>
            </Layout>
          </Row>
        </Layout>
        <Layout grow={1} />
        <Layout>
          <Footer />
        </Layout>
        <Layout basis='40px' />
      </Column>
      <Layout basis='10%' />
    </Row>
  </Block>
)
export default injectIntl(Main)

import { defineMessages } from 'react-intl'

export default defineMessages({
  welcome: {
    id: 'main.welcome',
    defaultMessage: 'Добро пожаловать в Hezzl.com',
  },
  dontHaveAnyCompany: {
    id: 'main.dont_have_any_company',
    defaultMessage: 'У вас еще нет ни одной кампании',
  },
  createCompany: {
    id: 'main.create_company',
    defaultMessage: 'Создать кампанию',
  },
})

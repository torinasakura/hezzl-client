import { createReducer } from '@hl/utils'
import * as actions from '../constants/notifications'

const initialState = []

export default createReducer(initialState, {
  [actions.send]: (state, { key, message, id, props }) => state.concat([{ key, message, id, ...props }]),
  [actions.dismiss]: (state, { key }) => state.filter(message => key !== message.key),
})

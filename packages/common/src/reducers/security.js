import { persistReducer } from 'redux-persist'
import { createReducer } from '@hl/utils'
import storage from 'redux-persist/es/storage'
import * as actions from '../constants/security'

const initialState = {
  token: '',
  expiredAt: '',
}

const reducer = createReducer(initialState, {
  [actions.login]: (state, { token, expiredAt }) => ({ ...state, token, expiredAt }),
  [actions.logout]: state => ({ ...state, token: '', expiredAt: '' }),
})

export default persistReducer({
  key: 'root',
  storage,
}, reducer)

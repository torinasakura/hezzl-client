import { connect } from 'react-redux'
import { dismiss } from '../actions/notifications'
import Notifications from '../components/Notifications'

export default connect(
  state => ({
    messages: state.notifications,
  }),
  dispatch => ({
    onDismiss: key => dispatch(dismiss(key)),
  }),
)(Notifications)

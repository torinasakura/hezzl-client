import React from 'react'
import { injectIntl } from 'react-intl'
import { Notification } from '@hl/ui/src/notification'

const Notifications = ({ intl, messages, onDismiss }) => (
  <Notification
    messages={messages.map(({ message, params, ...props }) => {
      if (message.id) {
        return {
          message: intl.formatMessage(message, params),
          ...props,
        }
      }

      return { message, params, ...props }
    })}
    onDismiss={onDismiss}
  />
)

export default injectIntl(Notifications)

import { getRandomId } from '@hl/utils'
import * as actions from '../constants/notifications'

export const dismiss = key => ({ type: actions.dismiss, key })

export const send = ({ key = getRandomId(), message, id, ...props }) => ({
  type: actions.send,
  key,
  message,
  id,
  props,
})

import * as actions from '../constants/security'

export const login = ({ token, expiredAt }) => ({
  type: actions.login,
  token,
  expiredAt,
})

export const logout = () => ({
  type: actions.logout,
})

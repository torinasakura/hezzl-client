import { connect } from 'react-redux'
import { addLocaleData, IntlProvider } from 'react-intl'
import enLocaleData from 'react-intl/locale-data/en'
import ruLocaleData from 'react-intl/locale-data/ru'

addLocaleData(enLocaleData)
addLocaleData(ruLocaleData)

export default connect(
  state => ({
    defaultLocale: 'ru',
    locale: state.locale.locale,
    messages: state.messages[state.locale.locale],
  }),
)(IntlProvider)

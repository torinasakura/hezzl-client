import { connect } from 'react-redux'
import { logout } from '@hl/common/src/actions/security'
import Sidebar from '../../components/desktop/Sidebar'
import { changeSidebar } from '../../actions/sidebar'

export default connect(
  state => ({
    company: state.sidebar.company,
    request: state.sidebar.request,
    name: state.init.user.name,
    email: state.init.user.email,
    phone: state.init.user.phone,
    organization: state.init.user.organization,
  }),
  dispatch => ({
    onSelectCompany: () => dispatch(changeSidebar('company')),
    onSelectRequest: () => dispatch(changeSidebar('request')),
    onLogout: () => dispatch(logout()),
  }),
)(Sidebar)

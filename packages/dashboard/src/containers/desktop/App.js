import React from 'react'
import { Row, Layout } from 'flex-layouts'
import { Block } from '@hl/ui/src/content'
import Notifications from '@hl/common/src/containers/Notifications'
import Sidebar from './Sidebar'

const App = ({
  children,
}) => (
  <Block>
    <Row fill>
      <Layout basis='145px'>
        <Sidebar />
      </Layout>
      <Layout justify='center' grow={1}>
        {children}
        <Notifications />
      </Layout>
    </Row>
  </Block>
)

export default App

import React from 'react'
import { Route, Switch } from 'react-router'
import Auth from '@hl/auth/src/containers/desktop/Auth'
import Main from '@hl/main/src/containers/desktop/Main'
import App from './App'

const Routes = () => (
  <Switch>
    <Route path='/auth' component={Auth} />
    <App>
      <Switch>
        <Route path='/' component={Main} />
      </Switch>
    </App>
  </Switch>
)

export default Routes

import { defineMessages } from 'react-intl'

export default defineMessages({
  company: {
    id: 'dashboard.sidebar.company',
    defaultMessage: 'кампания',
  },
  request: {
    id: 'dashboard.sidebar.request',
    defaultMessage: 'обращения',
  },
  createCompany: {
    id: 'dashboard.sidebar.create_company',
    defaultMessage: 'Создать компанию',
  },
  dontHaveAnyCompany: {
    id: 'dashboard.sidebar.dont_have_any_company',
    defaultMessage: 'У вас еще нет ни одной кампании',
  },
  settings: {
    id: 'dashboard.sidebar.settings',
    defaultMessage: 'Настройки',
  },
  changePassword: {
    id: 'dashboard.sidebar.change_password',
    defaultMessage: 'Изменить пароль',
  },
  logout: {
    id: 'dashboard.sidebar.logout',
    defaultMessage: 'Выйти из аккаунта',
  },
})

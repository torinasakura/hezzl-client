import { createReducer } from '@hl/utils'
import * as actions from '../constants/sidebar'

const initialState = {
  company: false,
  request: false,
}


export default createReducer(initialState, {
  [actions.change]: (state, { field, value }) => ({ ...state, [field]: value }),
})

import { createReducer } from '@hl/utils'
import { addLocaleData } from 'react-intl'
import ruLocaleData from 'react-intl/locale-data/ru'
import enLocaleData from 'react-intl/locale-data/en'
import en from '../../locales/en.json'
import ru from '../../locales/ru.json'

addLocaleData(ruLocaleData)
addLocaleData(enLocaleData)

const initialState = {
  en,
  ru,
}

export default createReducer(initialState, {})

import { createReducer } from '@hl/utils'
import * as actions from '../constants/init'

const initialState = {
  user: {},
}

export default createReducer(initialState, {
  [actions.loadUser]: (state, { user }) => ({ ...state, user }),
  [actions.clearUser]: () => ({ ...initialState }),
})

import { createReducer } from '@hl/utils'
import * as actions from '../constants/locale'

const initialState = {
  locale: 'ru',
}

export default createReducer(initialState, {
  [actions.change]: (state, { field, value }) => ({ ...state, [field]: value }),
})

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import auth from '@hl/auth/src/reducers'
import security from '@hl/common/src/reducers/security'
import notifications from '@hl/common/src/reducers/notifications'
import locale from './locale'
import messages from './messages'
import init from './init'
import sidebar from './sidebar'

export default history => combineReducers({
  router: connectRouter(history),
  auth,
  security,
  locale,
  messages,
  notifications,
  init,
  sidebar,
})

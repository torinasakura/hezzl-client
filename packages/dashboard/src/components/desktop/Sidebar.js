import React from 'react'
import { injectIntl } from 'react-intl'
import { Column, Row, Layout } from 'flex-layouts'
import { Background, Block } from '@hl/ui/src/content'
import { Sidebar as BaseSidebar, Elements as SidebarElements, Item } from '@hl/ui/src/sidebar'
import { HelpIcon } from '@hl/ui/src/icons'
import { Text } from '@hl/ui/src/text'
import { Button } from '@hl/ui/src/button'
import { Link } from '@hl/ui/src/link'
import messages from '../../messages/sidebar'
import avatar from '../../../assets/avatar.png'

const Sidebar = ({
  intl,
  company,
  request,
  name = '',
  email = '',
  phone = '',
  organization = '',
  onSelectCompany,
  onSelectRequest,
  onLogout,
}) => (
  <BaseSidebar>
    <Block
      height='fill'
      color='blue200'
    >
      <Column
        align='center'
      >
        <Layout basis='20px' />
        <Layout basis='40px'>
          <Background
            size='small'
            image={avatar}
          />
        </Layout>
        <Layout basis='110px' />
        <Link
          onClick={onSelectCompany}
        >
          <Column
            align='center'
          >
            <Layout>
              <HelpIcon
                width={40}
                color='orange'
              />
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Text
                size='small'
                color='orange'
              >
                {intl.formatMessage(messages.company)}
              </Text>
            </Layout>
          </Column>
        </Link>
        <Layout basis='40px' />
        <Link
          onClick={onSelectRequest}
        >
          <Column
            align='center'
          >
            <Layout>
              <HelpIcon
                color='gray'
                width={40}
              />
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Text
                size='small'
                color='gray500'
              >
                {intl.formatMessage(messages.request)}
              </Text>
            </Layout>
          </Column>
        </Link>
        <Layout grow='1' />
      </Column>
    </Block>
    <SidebarElements
      match={company}
    >
      <Column
        align='center'
      >
        <Layout basis='25px' />
        <Layout>
          <Row
            justify='center'
          >
            <Layout basis='400px'>
              <Button
                color='green'
              >
                {intl.formatMessage(messages.createCompany)}
              </Button>
            </Layout>
          </Row>
        </Layout>
        <Layout basis='110px' />
        <Layout>
          <Row justify='center'>
            <Layout basis='30px'>
              <HelpIcon
                width={30}
                color='gray'
              />
            </Layout>
            <Layout basis='20px' />
            <Layout basis='350px'>
              <Text
                weight='light'
                color='gray500'
              >
                {intl.formatMessage(messages.dontHaveAnyCompany)}
              </Text>
            </Layout>
          </Row>
        </Layout>
      </Column>
    </SidebarElements>
    <SidebarElements
      match={request}
    >
      <Column>
        <Layout basis='30px' />
        <Layout>
          <Row>
            <Layout basis='50px' />
            <Layout>
              <Text
                weight='light'
              >
                {name}
              </Text>
            </Layout>
            <Layout basis='50px' />
          </Row>
        </Layout>
        <Layout basis='40px' />
        <Item>
          {organization}
        </Item>
        <Item>
          {phone}
        </Item>
        <Item>
          {email}
        </Item>
        <Layout basis='40px' />
        <Layout basis='1px'>
          <Block
            color='gray500'
          />
        </Layout>
        <Layout basis='40px' />
        <Item
          active
        >
          {intl.formatMessage(messages.settings)}
        </Item>
        <Item
          active
        >
          {intl.formatMessage(messages.changePassword)}
        </Item>
        <Layout basis='40px' />
        <Layout basis='1px'>
          <Block
            color='gray500'
          />
        </Layout>
        <Layout basis='40px' />
        <Item
          active
          onClick={onLogout}
        >
          {intl.formatMessage(messages.logout)}
        </Item>
      </Column>
    </SidebarElements>
  </BaseSidebar>
)

export default injectIntl(Sidebar)

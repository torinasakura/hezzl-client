import axios from 'axios'
import * as actions from '../constants/init'

export const init = () => async (dispatch, getState) => {
  const { token } = getState().security

  const { data } = await axios({
    method: 'get',
    url: 'https://devops.hezzl.com/admin/info',
    headers: {
      'X-User-Token': token,
    },
  })

  dispatch({
    type: actions.loadUser,
    user: data,
  })
}

export const clear = () => (dispatch, getState, history) => {
  history.push('/auth')

  dispatch({
    type: actions.clearUser,
  })
}

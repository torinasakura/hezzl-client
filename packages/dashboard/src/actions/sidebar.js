import * as actions from '../constants/sidebar'

export const change = (field, value) => ({
  type: actions.change,
  field,
  value,
})

export const changeSidebar = field => (dispatch, getState) => {
  const { request, company } = getState().sidebar

  dispatch({
    type: actions.change,
    field: 'request',
    value: field === 'request' ? !request : false,
  })

  dispatch({
    type: actions.change,
    field: 'company',
    value: field === 'company' ? !company : false,
  })
}

import { change as localeAction } from '@hl/dashboard/src/constants/locale'

export const changeLocale = value => async (dispatch, getState) => {
  const { locale } = getState().locale

  if (locale !== value) {
    dispatch({
      type: localeAction,
      field: 'locale',
      value,
    })
  }
}

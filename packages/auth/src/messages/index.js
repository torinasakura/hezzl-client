import { defineMessages } from 'react-intl'

export default defineMessages({
  noAccount: {
    id: 'auth.no_account',
    defaultMessage: 'У вас нет аккаунта? ',
  },
  registration: {
    id: 'auth.registration',
    defaultMessage: 'Регистрация',
  },
  english: {
    id: 'auth.english',
    defaultMessage: 'English',
  },
  russian: {
    id: 'auth.russian',
    defaultMessage: 'Русский',
  },
})

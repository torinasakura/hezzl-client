import { connect } from 'react-redux'
import Header from '../../components/desktop/Header'
import { changeLocale } from '../../actions'

export default connect(
  state => ({
    locale: state.locale.locale,
  }),
  dispatch => ({
    onChangeRussianLocale: () => dispatch(changeLocale('ru')),
    onChangeEnglishLocale: () => dispatch(changeLocale('en')),
  }),
)(Header)

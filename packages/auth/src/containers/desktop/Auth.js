import React from 'react'
import { Route, Switch } from 'react-router-dom'
import App from './App'
import Login from '../../pages/login/containers/desktop/Login'

const Auth = () => (
  <App>
    <Switch>
      <Route path='/auth' exact component={Login} />
    </Switch>
  </App>
)

export default Auth

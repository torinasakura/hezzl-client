import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from '@hl/ui/src/content'
import Notifications from '@hl/common/src/containers/Notifications'
import Header from './Header'
import Footer from './Footer'

const App = ({
  children,
}) => (
  <Block
    color='gray600'
    height='fill'
  >
    <Column
      fill
    >
      <Layout>
        <Header />
      </Layout>
      <Layout>
        <Row justify='center'>
          {children}
        </Row>
      </Layout>
      <Layout>
        <Footer />
      </Layout>
      <Notifications />
    </Column>
  </Block>
)

export default App

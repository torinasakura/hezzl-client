import { combineReducers } from 'redux'
import login from '../pages/login/reducers'

export default combineReducers({
  login,
})

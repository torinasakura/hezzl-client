export const change = '@@hl/auth/login/CHANGE'
export const setErrors = '@@hl/auth/login/SET_ERRORS'
export const clear = '@@hl/auth/login/CLEAR'

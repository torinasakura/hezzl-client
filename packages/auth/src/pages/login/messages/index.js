import { defineMessages } from 'react-intl'

export default defineMessages({
  authorization: {
    id: 'auth.login.authorization',
    defaultMessage: 'Авторизация',
  },
  forgotPassword: {
    id: 'auth.login.forgot_password',
    defaultMessage: 'Забыли пароль?',
  },
  login: {
    id: 'auth.login.login',
    defaultMessage: 'Войти',
  },
  username: {
    id: 'auth.login.username',
    defaultMessage: 'Email',
  },
  password: {
    id: 'auth.login.password',
    defaultMessage: 'Password',
  },
})

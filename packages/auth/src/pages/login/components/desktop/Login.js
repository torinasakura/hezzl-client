import React from 'react'
import { injectIntl } from 'react-intl'
import { Column, Layout, Row } from 'flex-layouts'
import { Block } from '@hl/ui/src/content'
import { HintInput, Hint } from '@hl/ui/src/input'
import { Text } from '@hl/ui/src/text'
import { Link } from '@hl/ui/src/link'
import { Button } from '@hl/ui/src/button'
import { HelpIcon } from '@hl/ui/src/icons'
import messages from '../../messages'

const Login = ({
  intl,
  username,
  password,
  errors,
  onChangeUsername,
  onChangePassword,
  onLogin,
}) => (
  <Layout basis='825px'>
    <Block
      radius='medium'
    >
      <Row justify='center'>
        <Layout basis='650px'>
          <Column>
            <Layout basis='80px' />
            <Layout>
              <Text
                weight='light'
              >
                {intl.formatMessage(messages.authorization)}
              </Text>
            </Layout>
            <Layout basis='60px' />
            <Layout justify='center'>
              <HintInput
                leftHint={(
                  <Hint
                    align='left'
                  >
                    <Text
                      size='normal'
                    >
                      @
                    </Text>
                  </Hint>
                )}
                placeholder={intl.formatMessage(messages.username)}
                value={username}
                error={errors.email}
                onChange={onChangeUsername}
              />
            </Layout>
            <Layout basis='50px' />
            <Layout justify='center'>
              <HintInput
                leftHint={(
                  <Hint
                    align='left'
                  >
                    <HelpIcon
                      width={30}
                    />
                  </Hint>
                )}
                placeholder={intl.formatMessage(messages.password)}
                type='password'
                value={password}
                error={errors.password}
                onChange={onChangePassword}
              />
            </Layout>
            <Layout basis='80px' />
            <Layout>
              <Row
                align='center'
              >
                <Layout>
                  <Link to='/auth'>
                    <Text
                      color='gray500'
                      link
                    >
                      {intl.formatMessage(messages.forgotPassword)}
                    </Text>
                  </Link>
                </Layout>
                <Layout grow={1} />
                <Layout basis='205px'>
                  <Button
                    onClick={onLogin}
                  >
                    {intl.formatMessage(messages.login)}
                  </Button>
                </Layout>
              </Row>
            </Layout>
            <Layout basis='90px' />
          </Column>
        </Layout>
      </Row>
    </Block>
  </Layout>
)

export default injectIntl(Login)

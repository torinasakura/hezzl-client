import axios from 'axios'
import { send } from '@hl/common/src/actions/notifications'
import { login } from '@hl/common/src/actions/security'
import * as actions from '../constants'

export const change = (field, value) => ({
  type: actions.change,
  field,
  value,
})

export const signin = () => async (dispatch, getState) => {
  const { username, password } = getState().auth.login

  try {
    const { data } = await axios({
      method: 'POST',
      url: 'https://devops.hezzl.com/admin/login',
      data: `email=${username}&password=${password}`,
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })

    dispatch(login({
      token: data.token,
      expiredAt: data.expiredAt,
    }))
  } catch (error) {
    dispatch(
      send({
        message: 'Поля заполнены не корректно. Пожалуйста, попробуйте снова',
      }),
    )
  }
}

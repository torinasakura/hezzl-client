import React from 'react'
import { injectIntl } from 'react-intl'
import { Column, Row, Layout } from 'flex-layouts'
import { Text } from '@hl/ui/src/text'
import { Link } from '@hl/ui/src/link'
import { Background } from '@hl/ui/src/content'
import { Footer as FooterBase } from '@hl/ui/src/footer'
import recaptcha from '@hl/dashboard/assets/reCaptcha.png'
import messages from '../../messages'

const Footer = ({
  intl,
}) => (
  <Row>
    <Layout basis='10%' />
    <Layout basis='80%'>
      <Column>
        <Layout basis='80px' />
        <Layout justify='center'>
          <Row justify='center'>
            <Layout>
              <Text
                weight='light'
              >
                {intl.formatMessage(messages.noAccount)}
              </Text>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Link
                to='/auth'
              >
                <Text
                  weight='light'
                  color='gray500'
                  link
                >
                  {intl.formatMessage(messages.registration)}
                </Text>
              </Link>
            </Layout>
          </Row>
        </Layout>
        <Layout grow={1} />
        <Layout justify='end'>
          <Background
            size='medium'
            image={recaptcha}
          />
        </Layout>
        <Layout>
          <FooterBase />
        </Layout>
        <Layout basis='75px' />
      </Column>
    </Layout>
    <Layout basis='10%' />
  </Row>
)

export default injectIntl(Footer)

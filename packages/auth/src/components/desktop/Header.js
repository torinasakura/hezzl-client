import React from 'react'
import { injectIntl } from 'react-intl'
import { Column, Row, Layout } from 'flex-layouts'
import { HelpIcon } from '@hl/ui/src/icons'
import { Link } from '@hl/ui/src/link'
import { Text } from '@hl/ui/src/text'
import { Logo } from '@hl/ui/src/logo'
import messages from '../../messages'

const Header = ({
  intl,
  locale,
  onChangeEnglishLocale,
  onChangeRussianLocale,
}) => (
  <Row>
    <Layout basis='10%' />
    <Layout basis='80%'>
      <Column>
        <Layout basis='50px' />
        <Layout justify='end'>
          <Row justify='end'>
            <Layout>
              <HelpIcon width={30} />
            </Layout>
            <Layout basis='60px' />
            <Layout>
              <Link
                onClick={onChangeEnglishLocale}
              >
                <Text
                  color={locale !== 'en' ? 'gray500' : 'black'}
                  link={locale !== 'en'}
                >
                  {intl.formatMessage(messages.english)}
                </Text>
              </Link>
            </Layout>
            <Layout basis='10px' />
            <Layout>
              <Link
                onClick={onChangeRussianLocale}
              >
                <Text
                  color={locale !== 'ru' ? 'gray500' : 'black'}
                  link={locale !== 'ru'}
                >
                  {intl.formatMessage(messages.russian)}
                </Text>
              </Link>
            </Layout>
          </Row>
        </Layout>
        <Layout justify='center'>
          <Logo />
        </Layout>
      </Column>
    </Layout>
    <Layout basis='10%' />
  </Row>
)

export default injectIntl(Header)

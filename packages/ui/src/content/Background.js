import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    boxSizing: 'border-box',
    backgroundSize: 'cover',
  },
  'size=fill': {
    width: '100%',
    height: '100%',
  },
  'size=medium': {
    width: '130px',
    height: '130px',
  },
  'size=small': {
    width: '90px',
    height: '90px',
  },
})

const Background = ({
  image,
  radius,
  border,
  size = 'fill',
}) => (
  <div
    className={styles({
      radius,
      border,
      size,
    })}
    style={{ backgroundImage: image ? `url(${image})` : 'none' }}
  />
)

export default Background

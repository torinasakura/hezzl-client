# Контент

### Блок серый500
```jsx harmony
import React from 'react'
import Block from './Block'

const BlockExample = () => (
  <Block color='gray500'>
  <div style={{ margin: '10px', padding: '10px', width: '50px', height: '50px' }} />
  </Block>
)

export default BlockExample
```

### Блок серый600
```jsx harmony
import React from 'react'
import Block from './Block'

const BlockExample = () => (
  <Block color='gray600'>
  <div style={{ margin: '10px', padding: '10px', width: '50px', height: '50px' }} />
  </Block>
)

export default BlockExample
```

### Блок синий200
```jsx harmony
import React from 'react'
import Block from './Block'

const BlockExample = () => (
  <Block color='blue200'>
  <div style={{ margin: '10px', padding: '10px', width: '50px', height: '50px' }} />
  </Block>
)

export default BlockExample
```

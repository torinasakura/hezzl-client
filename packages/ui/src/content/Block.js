import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '100%',
    transition: '0.2s',
  },
  'height=fill': {
    minHeight: '100%',
  },
  'color=gray600': {
    backgroundColor: '#F6F7FB',
  },
  'color=gray500': {
    backgroundColor: '#E6ECF5',
  },
  'color=white': {
    backgroundColor: '#FFF',
  },
  'color=blue200': {
    backgroundColor: '#313643',
  },
  'radius=medium': {
    borderRadius: '10px',
  },
  'shadow=medium': {
    boxShadow: '0 3px 20px 0 rgba(0, 0, 0, 0.16)',
  },
})

const Block = ({
  children,
  color = 'white',
  radius,
  height,
}) => (
  <div
    className={styles({
      color,
      radius,
      height,
    })}
  >
    {children}
  </div>
)

export default Block

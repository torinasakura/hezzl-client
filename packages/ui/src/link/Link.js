import { createElement } from 'react'
import { NavLink as RouteLink } from 'react-router-dom'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    textDecoration: 'none',
    cursor: 'pointer',
  },
  'icon=right': {
    paddingRight: '8px',
    '& svg': {
      marginLeft: 'auto',
    },
  },
})

const Link = ({
  to = '',
  children,
  onClick,
  className,
  ...props
}) => createElement(
  to ? RouteLink : 'a',
  {
    ...props,
    className: className || styles(),
    onClick,
    to,
  },
  children,
)

export default Link

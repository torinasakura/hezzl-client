# Батн

### Батн
```jsx harmony
import React from 'react'
import Button from './Button'

const ButtonExample = () => (
  <div style={{ margin: '10px', padding: '10px' }}>
    <Button>
      Example
    </Button>
  </div>
)

export default ButtonExample
```

### Батн
```jsx harmony
import React from 'react'
import Button from './Button'

const ButtonExample = () => (
  <div style={{ margin: '10px', padding: '10px' }}>
    <Button
      color='green'
    >
      Example
    </Button>
  </div>
)

export default ButtonExample
```

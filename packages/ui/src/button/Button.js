/* eslint-disable react/button-has-type */
import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    padding: '0',
    boxSizing: 'border-box',
    border: 'none',
    outline: '0',
    transition: '0.2s',
    cursor: 'pointer',
  },
  'height=medium': {
    height: '80px',
  },
  'color=blue400': {
    backgroundColor: '#22A4DC',
  },
  'color=green': {
    backgroundColor: '#AAC72E',
  },
  'text=normal': {
    fontFamily: 'Roboto, sans-serif',
    fontSize: '30px',
    fontWeight: '300',
    lineHeight: 1.2,
    color: '#FFFFFF',
  },
  'radius=medium': {
    borderRadius: '10px',
  },
})

const Button = ({
  children,
  text = 'normal',
  color = 'blue400',
  height = 'medium',
  onClick,
  radius = 'medium',
  ...props
}) => (
  <button
    {...props}
    className={styles({
      text,
      color,
      height,
      radius,
    })}
    onClick={onClick}
  >
    {children}
  </button>
)

export default Button

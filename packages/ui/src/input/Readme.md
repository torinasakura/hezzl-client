# Инпат

### Инпат

```jsx harmony
import React from 'react'
import Input from './Input'

const InputExample = () => (
  <div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <Input />
    </div>
  </div>
)

export default InputExample
```


### Инпат

```jsx harmony
import React from 'react'
import { HintInput, Hint } from '../input'
import { Text } from '../text'

const InputExample = () => (
  <div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <HintInput
        leftHint={(
          <Hint
            align='left'
          >
            <Text
              size='normal'
            >
              @
            </Text>
          </Hint>
        )}
      />
    </div>
  </div>
)

export default InputExample
```

import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    justifyContent: 'center',
    width: '18px',
    marginRight: '12px',
  },
  minimized: {
    marginRight: 0,
  },
})

const Icon = ({ children, minimized }) => (
  <span className={styles({ minimized })}>
    {children}
  </span>
)

export default Icon

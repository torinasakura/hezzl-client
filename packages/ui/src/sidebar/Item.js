import React from 'react'
import { Layout } from 'flex-layouts'
import { StyleSheet } from 'elementum'
import { Link } from '../link'
import { HelpIcon } from '../icons'
import { Text } from '../text'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    flex: '0 0 70px',
    textDecoration: 'none',
    boxSizing: 'border-box',
  },
  active: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#E6ECF5',
    },
  },
})

const Item = ({
  to,
  exact,
  children,
  onClick,
  active,
}) => (
  <Link
    to={to}
    exact={exact}
    className={styles({
      active,
    })}
    onClick={onClick}
  >
    <Layout basis='50px' />
    <Layout>
      <HelpIcon
        color={active ? null : 'gray'}
        width={30}
      />
    </Layout>
    <Layout basis='30px' />
    <Layout>
      <Text
        color={active ? null : 'gray500'}
        weight='light'
      >
        {children}
      </Text>
    </Layout>
    <Layout basis='50px' />
  </Link>
)

export default Item

import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '145px',
    minHeight: '100vh',
    height: '100%',
    transition: 'all ease-in-out .2s',
    zIndex: 10,
  },
})

const Sidebar = ({ children }) => (
  <div className={styles()}>
    {children}
  </div>
)

export default Sidebar

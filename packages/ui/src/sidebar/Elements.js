import React from 'react'
import { StyleSheet } from 'elementum'
import { Condition } from '../condition'
import { Layer } from '../layer'

const styles = StyleSheet.create({
  self: {
    height: '100vh',
    width: '500px',
    boxShadow: '0 3px 20px 0 rgba(0, 0, 0, 0.16)',
    backgroundColor: '#FFF',
    zIndex: 15,
  },
})

const Container = ({ match, children }) => (
  <Condition match={match}>
    <Layer
      align='tl-tr'
    >
      <div
        className={styles()}
      >
        {children}
      </div>
    </Layer>
  </Condition>
)

export default Container

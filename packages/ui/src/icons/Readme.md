# Иконки

### Иконки
```jsx harmony
import React from 'react'
import HelpIcon from './HelpIcon'

const IconsExample = () => (
  <div style={{ margin: '10px', padding: '10px' }}>
    <HelpIcon />
  </div>
)

export default IconsExample
```

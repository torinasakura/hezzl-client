export const colors = {
  black: '#000',
  white: '#FFF',
  orange: '#F7AB29',
  blue: '#22A4DC',
  gray: '#ACAEB3',
}

import React, { Component } from 'react'
import { StyleSheet } from 'elementum'
import { TransitionMotion, spring } from 'react-motion'
import Notice from './Notice'

const styles = StyleSheet.create({
  self: {
    position: 'fixed',
    top: '150px',
    right: '50px',
    display: 'flex',
    flexDirection: 'column-reverse',
    zIndex: 1002,
  },
})

class Notification extends Component {
  onWillEnter() {
    return {
      opacity: 0,
      right: 0,
    }
  }

  onWillLeave() {
    return {
      opacity: spring(0),
      right: 16,
    }
  }

  getStyles(elements = {}) {
    return elements.map(node => ({
      key: node.key,
      style: {
        opacity: spring(1),
        right: spring(16),
      },
      data: node,
    }))
  }

  renderAnimationBlock = (elements) => {
    const { left, onDismiss } = this.props

    return (
      <div className={styles({ left })}>
        {elements.map(({ key, style, data: { message, type = 'default' } }) => (
          <Notice
            key={key}
            type={type}
            style={style}
            onClose={onDismiss && onDismiss.bind(this, key)}
          >
            {message}
          </Notice>
        ))}
      </div>
    )
  }

  render() {
    const { messages } = this.props

    return (
      <TransitionMotion
        styles={this.getStyles(messages)}
        willEnter={this.onWillEnter}
        willLeave={this.onWillLeave}
      >
        {this.renderAnimationBlock}
      </TransitionMotion>
    )
  }
}

export default Notification

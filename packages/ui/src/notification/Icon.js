import React from 'react'
import { StyleSheet } from 'elementum'
import { HelpIcon } from '../icons'

const styles = StyleSheet.create({
  self: {
    position: 'absolute',
    zIndex: 2,
  },
  'type=left': {
    top: '80px',
    left: '40px',
  },
  'type=right': {
    top: '80px',
    right: '40px',
  },
})

const Icon = ({ type }) => (
  <div className={styles({ type })}>
    <HelpIcon
      width={40}
      color={type === 'left' ? 'orange' : 'white'}
    />
  </div>
)

export default Icon

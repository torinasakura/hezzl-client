import React, { Component } from 'react'
import { StyleSheet } from 'elementum'
import Icon from './Icon'

const styles = StyleSheet.create({
  self: {
    position: 'relative',
    width: '800px',
    marginTop: '16px',
    padding: '40px 120px 30px',
    background: '#313643',
    border: '1px solid #E3E7F0',
    borderRadius: '10px',
    boxShadow: '0 3px 6px 0 rgba(0, 0, 0, 0.16)',
    boxSizing: 'border-box',
    fontFamily: 'Roboto, sans-serif',
    fontSize: '30px',
    lineHeight: 1.8,
    color: '#FFF',
  },
})

class Notice extends Component {
  state = {
    dismissTime: 3000,
    timeKiller: 0,
  }

  componentDidMount() {
    const { type, onClose } = this.props
    const step = 100

    this.setState({
      dismissTime: this.getDismissTime(type),
    })

    this.interval = setInterval(() => {
      const { dismissTime, timeKiller } = this.state

      if (timeKiller >= dismissTime) {
        clearInterval(this.interval)
        onClose()
      }
      this.setState({
        timeKiller: timeKiller + step,
      })
    }, step)
  }

  getDismissTime(type) {
    switch (type) {
      case 'error':
        return 7000
      default:
        return 3000
    }
  }

  render() {
    const { children, style, type } = this.props

    return (
      <div
        className={styles({ type })}
        style={{ ...style }}
        onMouseEnter={this.onMouseEnter}
      >
        <Icon type='left' />
        {children}
        <Icon type='right' />
      </div>
    )
  }
}

export default Notice

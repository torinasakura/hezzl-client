# Text

### Text

```jsx harmony
import React from 'react'
import { Text } from './index'

const TextExample = () => (
  <div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <Text>
        size: normal, color: black, weight: normal
      </Text>
    </div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <Text
        color='gray500'
        weight='light'
        size='small'
      >
        size: small, color: gray500, weight: light
      </Text>
    </div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <Text
        color='gray300'
        weight='medium'
        size='medium'
      >
        size: medium, color: gray300, weight: medium
      </Text>
    </div>
    <div style={{ margin: '10px', padding: '10px' }}>
      <Text
        color='orange'
        weight='bold'
        size='large'
      >
        size: large, color: orange, weight: bold
      </Text>
    </div>
  </div>
)

export default TextExample
```

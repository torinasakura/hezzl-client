import React from 'react'
import { StyleSheet } from 'elementum'

const styles = StyleSheet.create({
  self: {
    display: 'inline-flex',
  },
  'font=roboto': {
    fontFamily: 'Roboto, sans-serif',
  },
  'size=large': {
    fontSize: '44px',
  },
  'size=normal': {
    fontSize: '34px',
  },
  'size=medium': {
    fontSize: '30px',
  },
  'size=small': {
    fontSize: '20px',
  },
  'color=white': {
    color: '#FFF',
  },
  'color=black': {
    color: '#000',
  },
  'color=gray500': {
    color: '#888EA7',
  },
  'color=gray300': {
    color: '#707070',
  },
  'color=orange': {
    color: '#F7AB29',
  },
  'weight=light': {
    fontWeight: 300,
  },
  'weight=normal': {
    fontWeight: 400,
  },
  'weight=medium': {
    fontWeight: 500,
  },
  'weight=bold': {
    fontWeight: 700,
  },
  'align=center': {
    textAlign: 'center',
  },
  'lineHeight=normal': {
    lineHeight: 1.2,
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  lowercase: {
    textTransform: 'lowercase',
  },
  link: {
    textDecoration: 'underline',
  },
})

const Text = ({
  children,
  font = 'roboto',
  size = 'medium',
  color = 'black',
  weight = 'normal',
  lineHeight = 'normal',
  align,
  uppercase,
  lowercase,
  link,
  ...props
}) => (
  <span
    className={styles({
      font,
      size,
      color,
      weight,
      lineHeight,
      align,
      uppercase,
      lowercase,
      link,
    })}
    {...props}
  >
    {children}
  </span>
)

export default Text

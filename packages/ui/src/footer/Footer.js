import React from 'react'
import { StyleSheet } from 'elementum'
import { injectIntl } from 'react-intl'
import { Layout } from 'flex-layouts'
import { Link } from '../link'
import { Text } from '../text'
import messages from './messages'

const styles = StyleSheet.create({
  self: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    marginTop: '20px',
    paddingTop: '30px',
    borderTop: '1px solid #E6ECF5',
  },
})

const Footer = ({
  intl,
}) => (
  <div
    className={styles()}
  >
    <Layout>
      <Text
        color='gray500'
      >
        {intl.formatMessage(messages.copyright)}
      </Text>
    </Layout>
    <Layout grow={1} />
    <Layout>
      <Link>
        <Text
          color='gray500'
          link
        >
          {intl.formatMessage(messages.offer)}
        </Text>
      </Link>
    </Layout>
  </div>
)

export default injectIntl(Footer)

import { defineMessages } from 'react-intl'

export default defineMessages({
  copyright: {
    id: 'ui.footer.copyright',
    defaultMessage: '© 2019 ООО “Эпицентр" | Hezzl.com',
  },
  offer: {
    id: 'ui.footer.offer',
    defaultMessage: 'Оферта',
  },
})

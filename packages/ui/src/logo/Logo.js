import React from 'react'
import image from './Logo.png'

const Logo = () => (
  <img
    src={image}
    alt='logo'
  />
)

export default Logo
